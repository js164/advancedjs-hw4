// AJAX дозволяє робити запити на сервер точково і отримувати данні без перезавантаження сторінки.


function renderFilms(movie) {
    const filmList = document.createElement('div')
    const filmName = document.createElement('div')
    const episodeId = document.createElement('div')
    const opening = document.createElement('div')
    const charContainer = document.createElement('ul')

    filmName.innerText = movie.name
    episodeId.innerText = movie.id
    opening.innerText = movie.openingCrawl

    filmList.append(episodeId, filmName, opening, charContainer)

    document.body.append(filmList)

    const characterRequest = movie.characters.map(char => fetch(char).then(res => res.json()))

    Promise.all(characterRequest).then(chars => {
        chars.forEach(pers => {
            const character = document.createElement('li')
            character.innerText = pers.name
            charContainer.append(character)

        })
    })
}

fetch('https://ajax.test-danit.com/api/swapi/films').then(res => res.json()).then(movies => {
    movies.forEach(movie => renderFilms(movie) )
})


